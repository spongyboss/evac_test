package za.co.evac.app;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.device.ScanDevice;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.apextechnology.diskscan.DiskScanner;
import com.apextechnology.driverscan.activation.DriversScanner;
import com.apextechnology.driverscan.dto.DrivingLicenseCard;
import com.apextechnology.driverscan.dto.VehicleLicenseDisk;
import com.apextechnology.driverscan.helper.RawBitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "co.za.evac.app/broadcast_listeners";
    private final static String SCAN_ACTION = "com.android.serial.BARCODEPORT_RECEIVEDDATA_ACTION";
    public static final String DRIVER_SCANNER_API_KEY = "9JuNHwespfjN97tS5feMoCmIbrf7xr7krZUM68KnNHCtXKW3yFvUq8gv321W";

    private static final String TAG = MainActivity.class.getSimpleName();
//    private ScanDevice mScanDevice;
    private String barcodeStr;
    private VehicleLicenseDisk licenseDisk;
    private DrivingLicenseCard licenseCard;
    public static DriversScanner driversScanner;
    public static DiskScanner diskScanner;
    private static final int PERMISSION_READ_STATE = 0;
    MethodChannel.Result result;
    public final static int REQUEST_CODE = -1010101;
    // To keep track of activity's window focus
    boolean currentFocus;

    // To keep track of activity's foreground/background status
    boolean isPaused;

    Handler collapseNotificationHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
                PERMISSION_READ_STATE);
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result res) {
                        result = res;

                        IntentFilter filter = new IntentFilter();
                        switch (methodCall.method) {
                            case "registerDiskBroadcastListener":
                                Log.d("MainActivity", "register disk listener!!!!---");
//                                initScan();
                                filter.addAction(SCAN_ACTION);
                                registerReceiver(diskScanReceiver, filter);
                                break;
                            case "registerDriversBroadcastListener":
                                Log.d("MainActivity", "register drivers listener!!!!---");
//                                initScan();
                                filter.addAction(SCAN_ACTION);
                                registerReceiver(driversScanReceiver, filter);
                                break;
                            case "registerIDNumberBroadcastListener":
                                Log.d("MainActivity", "register id number listener!!!!---");
//                                initScan();
                                filter.addAction(SCAN_ACTION);
                                registerReceiver(idNumberScanReceiver, filter);
                                break;
                            case "unregisterDiskBroadcastListener":
                                try {
                                    unregisterReceiver(diskScanReceiver);
                                    Log.d(TAG, "disk unregistered");
                                } catch (IllegalArgumentException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case "unregisterDriversBroadcastListener":
                                try {
                                    unregisterReceiver(driversScanReceiver);
                                    Log.d(TAG, "drivers unregustered");
                                } catch (IllegalArgumentException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case "unregisterIDNumberBroadcastListener":
                                try {
                                    unregisterReceiver(idNumberScanReceiver);
                                    Log.d(TAG, "id Number unregustered");
                                } catch (IllegalArgumentException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case "disablePullDown":
                                checkDrawOverlayPermission();
                                break;
                            default:
                                result.notImplemented();
                                break;
                        }
                    }
                });
    }

//    private void initScan() {
//        mScanDevice.setOutScanMode(0);
//        mScanDevice.openScan();
//    }

    private BroadcastReceiver diskScanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            Log.d("Broadcast Receiver", "++++received");
            // isScanning = false;
            //soundpool.play(soundid, 1, 1, 0, 0, 1);
            //mVibrator.vibrate(100);


            byte[] barcode = intent.getByteArrayExtra("barocode");
//            Log.d(TAG, " ~~~ Scanner Type: " + mScanDevice.getScannerType() + " === " + mScanDevice.getScanCodeValue());
            int barcodelen = intent.getIntExtra("length", 0);
            byte temp = intent.getByteExtra("barcodeType", (byte) 0);
            Log.i("debug", "----codetype--" + temp);
            barcodeStr = new String(barcode, 0, barcodelen);
            Log.d("+++", barcodeStr);

            try {
                JSONObject jsonObject = new JSONObject(barcodeStr);
                Log.d(TAG, "a QR was scanned");
                result.success(jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
                try {
                    long number = Long.parseLong(barcodeStr);
                    Log.d(TAG,"normal barcode scanned");
                    result.success(number);
                }catch(NumberFormatException ex) {
                    ex.printStackTrace();
                    onDiskDetected(barcodeStr);
                }
            }
//            mScanDevice.stopScan();


        }

    };

    private BroadcastReceiver idNumberScanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            Log.d("Broadcast Receiver", "++++received"+intent.toString());


            byte[] barcode = intent.getByteArrayExtra("barocode");
//            Log.d(TAG, " ~~~ Scanner Type: " + mScanDevice.getScannerType() + " === " + mScanDevice.getScanCodeValue());
            int barcodelen = intent.getIntExtra("length", 0);
            byte temp = intent.getByteExtra("barcodeType", (byte) 0);
            byte[] aimid = intent.getByteArrayExtra("aimid");
            Log.i("debug", "----codetype--" + temp);
            Log.i("debug", "----amid--" + aimid);
            barcodeStr = new String(barcode, 0, barcodelen);
            Log.d("+++", barcodeStr);
            try {
                JSONObject jsonObject = new JSONObject(barcodeStr);
                Log.d(TAG, "a QR was scanned");
                result.success(jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
                try {
                    long number = Long.parseLong(barcodeStr);
                    Log.d(TAG,"normal barcode scanned");
                    result.success(number);
                }catch(NumberFormatException ex) {
                    ex.printStackTrace();
                    onDriversDetected(bytesToHexString(barcode));
                }
            }
//            mScanDevice.stopScan();


        }

    };



    private BroadcastReceiver driversScanReceiver = new BroadcastReceiver() {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            Log.d("Broadcast Receiver", "++++received"+intent.toString());


            String results =  intent.getStringExtra("DATA") + "\n";
            byte[] barcode = intent.getByteArrayExtra("barocode");
//            Log.d(TAG, " ~~~ Scanner Type: " + mScanDevice.getScannerType() + " === " + mScanDevice.getScanCodeValue());
//            int barcodelen = intent.getIntExtra("length", 0);
//            byte temp = intent.getByteExtra("barcodeType", (byte) 0);
//            byte[] aimid = intent.getByteArrayExtra("aimid");
//            Log.i("debug", "----codetype--" + temp);
//            Log.i("debug", "----amid--" + aimid);
//            barcodeStr = new String(barcode, 0, barcodelen);
            Log.d("+++", results);
            onDriversDetected(bytesToHexString(results.getBytes(StandardCharsets.UTF_8)));
         
//            mScanDevice.stopScan();


        }

    };

    private static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    private void onDiskDetected(final String barcodeString) {
        try {
            licenseDisk = diskScanner.doScan(barcodeString);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("registration_number", licenseDisk.getVehicleRegisterNumber());
            jsonObject.put("license_number", licenseDisk.getLicenseNumber());
            jsonObject.put("expiry_date", licenseDisk.getExpiryDate());
            jsonObject.put("make", licenseDisk.getMake());
            jsonObject.put("series", licenseDisk.getSeriesName());
            jsonObject.put("color", licenseDisk.getColour());
            jsonObject.put("engine_number", licenseDisk.getEngineNumber());
            jsonObject.put("description", licenseDisk.getVehicleDescription());
            Log.d(TAG, jsonObject.toString());
            result.success(jsonObject.toString());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            result.error("", "", "");
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private void onDriversDetected(final String barcodeString) {
        try {
            licenseCard = driversScanner.doScan(barcodeString);
            Bitmap bitmap = RawBitmap.toBitmap(licenseCard.getPhoto(), true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id_number", licenseCard.getIdentityDocument().getNumber());
            jsonObject.put("expiry_date", licenseCard.getCard().getValidUntil());
            jsonObject.put("photo", Base64.encodeToString(byteArrayImage, Base64.NO_WRAP));
            jsonObject.put("surname", licenseCard.getPerson().getSurname());
            jsonObject.put("gender", licenseCard.getPerson().getGender());
            jsonObject.put("cert_number", licenseCard.getDrivingLicense().getCertificateNumber());
            jsonObject.put("initials", licenseCard.getPerson().getInitials());
            jsonObject.put("issue_country", licenseCard.getDrivingLicense().getCountryOfIssue());
            result.success(jsonObject.toString());
            Log.d(TAG, jsonObject.toString());
        } catch (Exception e) {
            Log.d(TAG, ">>>>>" + e.getMessage());
            result.error("", "", "");
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_READ_STATE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //////////
                    Log.d(TAG,"****** We initializing");
//                    mScanDevice = new ScanDevice();
                    // Initiating Driver PDF417 Decryptor
                    driversScanner = new DriversScanner(this, DRIVER_SCANNER_API_KEY);
                    diskScanner = new DiskScanner(this, DRIVER_SCANNER_API_KEY);
                } else {
                    // permission denied
                }
            }

        }
    }

    public void collapseNow() {

        // Initialize 'collapseNotificationHandler'
        if (collapseNotificationHandler == null) {
            collapseNotificationHandler = new Handler();
        }

        // If window focus has been lost && activity is not in a paused state
        // Its a valid check because showing of notification panel
        // steals the focus from current activity's window, but does not
        // 'pause' the activity
        if (!currentFocus && !isPaused) {

            // Post a Runnable with some delay - currently set to 300 ms
            collapseNotificationHandler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    // Use reflection to trigger a method from 'StatusBarManager'

                    Object statusBarService = getSystemService("statusbar");
                    Class<?> statusBarManager = null;

                    try {
                        statusBarManager = Class.forName("android.app.StatusBarManager");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    Method collapseStatusBar = null;

                    try {

                        // Prior to API 17, the method to call is 'collapse()'
                        // API 17 onwards, the method to call is `collapsePanels()`

                        if (Build.VERSION.SDK_INT > 16) {
                            collapseStatusBar = statusBarManager.getMethod("collapsePanels");
                        } else {
                            collapseStatusBar = statusBarManager.getMethod("collapse");
                        }
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }

                    collapseStatusBar.setAccessible(true);

                    try {
                        collapseStatusBar.invoke(statusBarService);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    // Check if the window focus has been returned
                    // If it hasn't been returned, post this Runnable again
                    // Currently, the delay is 100 ms. You can change this
                    // value to suit your needs.
                    if (!currentFocus && !isPaused) {
                        collapseNotificationHandler.postDelayed(this, 100L);
                    }

                }
            }, 300L);
        }
    }

    /**
     * code to post/handler request for permission
     */

    @TargetApi(Build.VERSION_CODES.M)
    public void checkDrawOverlayPermission() {
        /* check if we already  have permission to draw over other apps */
        if (!Settings.canDrawOverlays(this)) {
            Log.d("PERM", "permission not granted");
            /* if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            /* request permission via start activity for result */
            startActivityForResult(intent, REQUEST_CODE);
        } else {
            disablePullNotificationTouch();
        }
    }

    private void disablePullNotificationTouch() {
        Log.d("JAVA CODE", "Calling method");
        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));
        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (25 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.RGBX_8888;
        CustomViewGroup view = new CustomViewGroup(this);
        manager.addView(view, localLayoutParams);

    }

    @Override
    protected void onPause() {
        super.onPause();

        // Activity's been paused
        isPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Activity's been resumed
        isPaused = false;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
    /* check if received result code
     is equal our requested code for draw permission  */
        if (requestCode == REQUEST_CODE) {
            if (Settings.canDrawOverlays(this)) {
                disablePullNotificationTouch();
                // continue here - permission was granted
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        currentFocus = hasFocus;

        if (!hasFocus) {

            // Method that handles loss of window focus
            collapseNow();
        }
    }

}
